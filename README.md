Trading View Pine indicator script to be added to Bitcoin pairs i.e. BTC/USD, XBT/GBP, etc. Works best as default in 'dark mode'.  

Features include:  

• Vwap + funding.  
• 'Bullish'/'bearish' indicators using on-chain data.  
• London, New York, Tokyo first 2 session hours highlighted.  
• Week CME session start and end.  
• 7d -14d VWAP.  
• Bitmex funding 8h rotations script (nice for scalping).  

This is a work in progress and  will update here: https://bitbucket.org/thisisbullish/tib/

// Contact: hello@bullish.design  
// © thisisbullish   
