// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/

//@version=4
study("BSI", overlay = true)

//Colors
green = #ffc60a
red = #e70e02
greenbg = #ffdc5e
redbg = #f42b03

// Inputs
priceCalculation = input(hl2, title="Calculation Method")
rangePeriods = input(title="Average Range Periods", type=input.integer, defval=8)
rangeMultiplier = input(title="Multiplier", type=input.float, step=0.1, defval=2)
showMoonSignal = input(title="Show Bullish/Wait ?", type=input.bool, defval=true)
highlights = input(title="Highlighter On/Off ?", type=input.bool, defval=true)
activeAddresses = quandl("BCHAIN/NADDU", barmerge.gaps_on, 0)
enableOnchain = input(title="Enable OnChain", defval = true, type=input.bool)

// Trend Variables & Calc
atr = atr(rangePeriods)
upTrend = priceCalculation-(rangeMultiplier*atr)
timeToMoon = nz(upTrend[1],upTrend)
upTrend := close[1] > timeToMoon ? max(upTrend,timeToMoon) : upTrend
downTrend = priceCalculation+(rangeMultiplier*atr)
downTrend1 = nz(downTrend[1], downTrend)
downTrend := close[1] < downTrend1 ? min(downTrend, downTrend1) : downTrend
trend = 1
trend := nz(trend[1], trend)
trend := trend == -1 and close > downTrend1 ? 1 : trend == 1 and close < timeToMoon ? -1 : trend
bullishZone = trend == 1 and trend[1] == -1
waitSignal = trend == -1 and trend[1] == 1

priceMaLong = ema(priceCalculation, 300)
priceMaShort = ema(priceCalculation, 150)
addressesMaLong = ema(activeAddresses, 400)
addressesMaShort = ema(activeAddresses, 200)
combinedLine = (addressesMaShort-addressesMaLong)
positive_cloud = priceMaShort > priceMaLong
negative_cloud = priceMaShort < priceMaLong
onchain_activity_bear = (combinedLine / combinedLine[15] > 1.2) and combinedLine > 0
onchain_activity_bull = (combinedLine[15] / combinedLine > 1.2) and combinedLine < 0

// Plots
downTrendPlot = plot(trend == 1 ? na : downTrend, title="Bearish", style=plot.style_linebr, linewidth=2, color=red)
upPlot = plot(trend == 1 ? upTrend : na, title="Up Trend", style=plot.style_linebr, linewidth=2, color=green)
mPlot = plot(ohlc4, title="", style=plot.style_circles, linewidth=0)
moonFillColor = highlights ? (trend == 1 ? green : color.white) : color.white
plotshape(waitSignal and showMoonSignal ? downTrend : na, title="Bearish", text="Bearish", location=location.absolute, style=shape.labeldown, size=size.tiny, color=red, textcolor=color.white, transp=0)
plotshape(bullishZone ? upTrend : na, title="UpTrend Begins", location=location.absolute, style=shape.circle, size=size.tiny, color=green, transp=0)
plotshape(bullishZone and showMoonSignal ? upTrend : na, title="Bullish", text="Bullish", location=location.absolute, style=shape.labelup, size=size.tiny, color=green, textcolor=color.white, transp=0)
plotshape(waitSignal ? downTrend : na, title="Waiting Begins", location=location.absolute, style=shape.circle, size=size.tiny,  transp=0)
fill(mPlot, upPlot, title="Bullish Highlight", color=moonFillColor)

plotLong = plot(priceMaLong, color=color.red, transp=100)
plotShort = plot(priceMaShort, transp=100)
fill(plotLong, plotShort, positive_cloud ? greenbg : redbg, transp=60)
plotshape(enableOnchain and onchain_activity_bear and onchain_activity_bear[1] and onchain_activity_bear[2] and onchain_activity_bear[3], style=shape.circle, color= red, size=size.small, location=location.bottom)
plotshape(enableOnchain and onchain_activity_bull and onchain_activity_bull[1] and onchain_activity_bull[2] and onchain_activity_bull[3] , style=shape.circle, color= green, size=size.small, location=location.bottom)

// Create the inputs
//frankfurtSession = input(title="Frankfurt session", type=input.session, defval="0200-1000")  //NY time
londonSession = input(title="London session", type=input.session, defval="0800-1000")
nySession = input(title="New York session", type=input.session, defval="1330-1500")
//sydneySession = input(title="Sydney session", type=input.session, defval="1800-0200")
tokyoSession = input(title="Tokyo session", type=input.session, defval="0000-0200")

transparency = input(title="Transparency", type=input.integer, defval=85, minval=0, maxval=100)

showDOW = input(true, title="Show day of week")

LS=time(timeframe.period, londonSession + ":1234567")
NYS=time(timeframe.period, nySession + ":1234567")
//SS=time(timeframe.period, sydneySession + ":1234567")
TS=time(timeframe.period,tokyoSession  + ":1234567")

// The BarInSession function returns true when
// the current bar is inside the session parameter
BarInSession(sess) =>
    not na(time(timeframe.period, sess))

// Set the colour variables for the ranges
//frankfurtColour = #6495ED  // Cornflower blue
londonColour = #98FB98  // Pale green
nyColour = #FB9898  // Pale red
//sydneyColour = color.orange
tokyoColour = color.fuchsia

// Determine the chart's background colour
//chartColour1 = BarInSession(frankfurtSession) ? frankfurtColour : na
//bgcolor(color=chartColour1, transp=transparency, title="Frankfurt session")

A= LS
B= NYS
//C= BarInSession(sydneySession) ? sydneyColour : na
D= TS

alertcondition(A, message="London open", title="London Open Alert")
alertcondition(B, message="New York open", title="New York Open Alert")
//alertcondition(C, message="Sydney open", title="Sydney Open Alert")
alertcondition(D, message="Tokyo open", title="Tokyo Open Alert")

chartColour2 = BarInSession(londonSession) ? londonColour : na
bgcolor(color=chartColour2, transp=transparency, title="London session")

chartColour3 = BarInSession(nySession) ? nyColour : na
bgcolor(color=chartColour3, transp=transparency, title="New York session")

//chartColour4 = BarInSession(sydneySession) ? sydneyColour : na
//bgcolor(color=chartColour4, transp=transparency, title="Sydney session")

chartColour5 = BarInSession(tokyoSession) ? tokyoColour : na
bgcolor(color=chartColour5, transp=transparency, title="Tokyo session")

plotshape(showDOW ? hour == 4 and minute == 0 and dayofweek == dayofweek.monday : false, text="Funding", color=color.lime, offset=0, style=shape.arrowdown, textcolor=color.lime, size=size.normal, transp=0)
plotshape(showDOW ? hour == 4 and minute == 0 and dayofweek == dayofweek.tuesday : false, text="Funding", color=color.lime, offset=0, style=shape.arrowdown, textcolor=color.lime, size=size.normal, transp=0)
plotshape(showDOW ? hour == 4 and minute == 0 and dayofweek == dayofweek.wednesday : false, text="Funding", color=color.lime, offset=0, style=shape.arrowdown, textcolor=color.lime, size=size.normal, transp=0)
plotshape(showDOW ? hour == 4 and minute == 0 and dayofweek == dayofweek.thursday : false, text="Funding", color=color.lime, offset=0, style=shape.arrowdown, textcolor=color.lime, size=size.normal, transp=0)
plotshape(showDOW ? hour == 4 and minute == 0 and dayofweek == dayofweek.friday : false, text="Funding", color=color.lime, offset=0, style=shape.arrowdown, textcolor=color.lime, size=size.normal, transp=0)
plotshape(showDOW ? hour == 4 and minute == 0 and dayofweek == dayofweek.saturday : false, text="Funding", color=color.lime, offset=0, style=shape.arrowdown, textcolor=color.lime, size=size.normal, transp=0)
plotshape(showDOW ? hour == 4 and minute == 0 and dayofweek == dayofweek.sunday : true, text="Funding", color=color.lime, offset=0, style=shape.arrowdown, textcolor=color.lime, size=size.normal, transp=0)

plotshape(showDOW ? hour == 12 and minute == 0 and dayofweek == dayofweek.monday : false, text="Funding", color=color.white, offset=0, style=shape.arrowdown, textcolor=color.white, size=size.normal, transp=0)
plotshape(showDOW ? hour == 12 and minute == 0 and dayofweek == dayofweek.tuesday : false, text="Funding", color=color.white, offset=0, style=shape.arrowdown, textcolor=color.white, size=size.normal, transp=0)
plotshape(showDOW ? hour == 12 and minute == 0 and dayofweek == dayofweek.wednesday : false, text="Funding", color=color.white, offset=0, style=shape.arrowdown, textcolor=color.white, size=size.normal, transp=0)
plotshape(showDOW ? hour == 12 and minute == 0 and dayofweek == dayofweek.thursday : false, text="Funding", color=color.white, offset=0, style=shape.arrowdown, textcolor=color.white, size=size.normal, transp=0)
plotshape(showDOW ? hour == 12 and minute == 0 and dayofweek == dayofweek.friday : false, text="Funding", color=color.white, offset=0, style=shape.arrowdown, textcolor=color.white, size=size.normal, transp=0)
plotshape(showDOW ? hour == 12 and minute == 0 and dayofweek == dayofweek.saturday : false, text="Funding", color=color.white, offset=0, style=shape.arrowdown, textcolor=color.white, size=size.normal, transp=0)
plotshape(showDOW ? hour == 12 and minute == 0 and dayofweek == dayofweek.sunday : true, text="Funding", color=color.white, offset=0, style=shape.arrowdown, textcolor=color.white, size=size.normal, transp=0)


plotshape(showDOW ? hour == 21 and minute == 0 and dayofweek == dayofweek.friday : false, text="CME close", color=color.red, offset=0, style=shape.arrowdown, textcolor=color.red, size=size.normal, transp=0)
plotshape(showDOW ? hour == 22 and minute == 0 and dayofweek == dayofweek.sunday : false, text="CME open", color=color.green, offset=0, style=shape.arrowdown, textcolor=color.green, size=size.normal, transp=0)

title1=            input(title = "═════════════════ VWAP settings ══════════════", defval = true, type = input.bool)

std1 = input(title = "STDEV1", defval = 1, type = input.float, step = 0.1)
std2 = input(title = "STDEV2", defval = 2, type = input.float, step = 0.1)
std3 = input(title = "STDEV3", defval = 3, type = input.float, step = 0.1)

showStd = input(false, title = "Show STDEV Bands")
showPrev = input(false, title = "Show Previous VWAP")
showPrevBand = input(false, title = "Show Previous STDEV = 1 Bands")
bands = input(title = "STDEV Bands Timeframe", defval = "1. 14Day", options=["1. 14Day", "2. Week", "3. Month", "4. 60 Day", "5. Year"])

dLabel = input(true, "14 Day VWAP Label")
wLabel = input(true, "Week VWAP Label")
mLabel = input(false, "Month VWAP Label")
qLabel = input(false, "60 Day VWAP Label")
yLabel = input(false, "Year VWAP Label")

textC = input(title = "Text Color", defval = "White", options=["White", "Black", "Red", "Green", "Blue"])
size = input(2, title = "Label Size (0 = Auto, 1 - 5)", minval = 0, maxval = 5)
_opacity1 = input(50, title = "Label Opacity (0 - 100)", minval = 0, maxval = 100, step = 10)
_opacity2 = input(100, title = "Line Opacity (0 - 100)", minval = 0, maxval = 100, step = 10)
lstyle = input(title = "Line Style", defval = "Dashed", options=["Solid", "Dotted", "Dashed"])
lineWidth = input(2, title = "Line Thickness (1 - 4)", minval = 1, maxval = 4)

opacity = floor(_opacity1 / 10) * 10
opacity2 = floor(_opacity2 / 10) * 10

offset = input(0, title = "Label Offset", minval = 0, maxval = 1000)

showPrice = input(true, "Show Price Values?")
showSessType = input(true, "Show Session Type?")

rez1 = iff(bands == "1. 14 Day", "14D", iff(bands == "2. Week", "W", iff(bands == "3. Month", "M", iff(bands == "4. 60 Day", "60D", "12M"))))

startD = security(syminfo.tickerid, "14D", time, lookahead = true)
startW = security(syminfo.tickerid, "W", time, lookahead = true)
startM = security(syminfo.tickerid, "M", time, lookahead = true)
startQ = security(syminfo.tickerid, "60D", time, lookahead = true)
startY = security(syminfo.tickerid, "12M", time, lookahead = true)

newSessionD = iff(change(startD), 1, 0)
newSessionW = iff(change(startW), 1, 0)
newSessionM = iff(change(startM), 1, 0)
newSessionQ = iff(change(startQ), 1, 0)
newSessionY = iff(change(startY), 1, 0)

getVWAP(newSession) =>
    p = hlc3 * volume
    p := newSession ? hlc3 * volume : p[1] + hlc3 * volume

    vol = 0.0
    vol := newSession ? volume : vol[1] + volume

    v = p / vol

    // Incremental weighted standard deviation (rolling)
    // http://people.ds.cam.ac.uk/fanf2/hermes/doc/antiforgery/stats.pdf (part 5)
    // x[i] = hlc3[i], w[i] = volume[i], u[i] - v[i]

    Sn = 0.0
    Sn := newSession ? 0 : Sn[1] + volume * (hlc3 - v[1]) * (hlc3 - v)
    std = sqrt(Sn / vol)

    [v, std]

[vD, stdevD] = getVWAP(newSessionD)
[vW, stdevW] = getVWAP(newSessionW)
[vM, stdevM] = getVWAP(newSessionM)
[vQ, stdevQ] = getVWAP(newSessionQ)
[vY, stdevY] = getVWAP(newSessionY)

dColor = #008000FF
wColor = #008080DD
mColor = #808000FF
qColor = #801010FF
yColor = #091580FF

vDplot = plot(vD, title = "VWAP 14D", color = #00FF00FF, style = plot.style_circles, transp = 10, linewidth = 1)
vWplot = plot(vW, title = "VWAP W", color = #00E2E2FF, style = plot.style_circles, transp = 10, linewidth = 1)
vMplot = plot(vM, title = "WVAP M", color = #C0C000FF, style = plot.style_circles, transp = 10, linewidth = 1)
vQplot = plot(vQ, title = "VWAP 60D", color = #BF1111FF, style = plot.style_circles, transp = 10, linewidth = 1)
vYplot = plot(vY, title = "VWAP Y", color = #0921FFDD, style = plot.style_circles, transp = 10, linewidth = 1)

v = iff(rez1 == "W", vW, iff(rez1 == "M", vM, iff(rez1 == "60D", vQ, iff(rez1 == "12M", vY, vD))))
s = iff(rez1 == "W", stdevW, iff(rez1 == "M", stdevM, iff(rez1 == "60D", stdevQ, iff(rez1 == "12M", stdevY, stdevD))))
vPlot = plot(v, title = "VWAP - Selected (transparent, not used)", color = #00000000)

s1up = plot(showStd ? v + std1 * s : na, title = "STDEV1", color = #00FFFFDD, linewidth = 1)
s1dn = plot(showStd ? v - std1 * s : na, title = "STDEV1", color = #00FFFFDD, linewidth = 1)
s2up = plot(showStd ? v + std2 * s : na, title = "STDEV2", color = #FF00FFDD, style = plot.style_circles, linewidth = 1)
s2dn = plot(showStd ? v - std2 * s : na, title = "STDEV2", color = #FF00FFDD, style = plot.style_circles, linewidth = 1)
s3up = plot(showStd ? v + std3 * s : na, title = "STDEV3", color = #CCCCCCDD, style = plot.style_circles, linewidth = 1)
s3dn = plot(showStd ? v - std3 * s : na, title = "STDEV3", color = #CCCCCCDD, style = plot.style_circles, linewidth = 1)

fill(vPlot, s1up, title = "VWAP - S1 Fill (Up)", color = #00FFFF1C)
fill(vPlot, s1dn, title = "VWAP - S1 Fill (Down)", color = #00FFFF1C)

fill(s1up, s2up, title = "S1 - S2 Fill (Up)", color = #FF00FF1C)
fill(s1dn, s2dn, title = "S1 - S2 Fill (Down)", color = #FF00FF1C)

fill(s2up, s3up, title = "S2 - S3 Fill (Up)", color = #CCCCCC1C)
fill(s2dn, s3dn, title = "S2 - S3 Fill (Down)", color = #CCCCCC1C)

pvD = valuewhen(newSessionD, vD[1], 0)
pvW = valuewhen(newSessionW, vW[1], 0)
pvM = valuewhen(newSessionM, vM[1], 0)
pvQ = valuewhen(newSessionQ, vQ[1], 0)
pvY = valuewhen(newSessionY, vY[1], 0)

plot(showPrev ? pvD : na, title = "pdVWAP", color = #00FF0099, style = plot.style_cross, transp = 10, linewidth = 1)
plot(showPrev ? pvW : na, title = "pwVWAP", color = #40E20099, style = plot.style_cross, transp = 10, linewidth = 2)
plot(showPrev ? pvM : na, title = "pmVWAP", color = #80C60099, style = plot.style_cross, transp = 10, linewidth = 3)
plot(showPrev ? pvQ : na, title = "PqVWAP", color = #BFA90099, style = plot.style_cross, transp = 10, linewidth = 4)
plot(showPrev ? pvY : na, title = "pyVWAP", color = #FF8C0099, style = plot.style_cross, transp = 10, linewidth = 4)

pvDS1up = valuewhen(newSessionD, vD[1] + std1 * stdevD[1], 0)
pvDS1dn = valuewhen(newSessionD, vD[1] - std1 * stdevD[1], 0)
pvWS1up = valuewhen(newSessionW, vW[1] + std1 * stdevW[1], 0)
pvWS1dn = valuewhen(newSessionW, vW[1] - std1 * stdevW[1], 0)
pvMS1up = valuewhen(newSessionM, vM[1] + std1 * stdevM[1], 0)
pvMS1dn = valuewhen(newSessionM, vM[1] - std1 * stdevM[1], 0)
pvQS1up = valuewhen(newSessionQ, vQ[1] + std1 * stdevQ[1], 0)
pvQS1dn = valuewhen(newSessionQ, vQ[1] - std1 * stdevQ[1], 0)
pvYS1up = valuewhen(newSessionY, vY[1] + std1 * stdevY[1], 0)
pvYS1dn = valuewhen(newSessionY, vY[1] - std1 * stdevY[1], 0)

plot(showPrevBand ? pvDS1up : na, title = "Previous VWAP S1 - Daily", color = #00FF0099, style = plot.style_cross, transp = 10, linewidth = 1)
plot(showPrevBand ? pvDS1dn : na, title = "Previous VWAP S1 - Daily", color = #00FF0099, style = plot.style_cross, transp = 10, linewidth = 1)
plot(showPrevBand ? pvWS1up : na, title = "Previous VWAP S1 - Weekly", color = #40E20099, style = plot.style_cross, transp = 10, linewidth = 2)
plot(showPrevBand ? pvWS1dn : na, title = "Previous VWAP S1 - Weekly", color = #40E20099, style = plot.style_cross, transp = 10, linewidth = 2)
plot(showPrevBand ? pvMS1up : na, title = "Previous VWAP S1 - Monthly", color = #80C60099, style = plot.style_cross, transp = 10, linewidth = 3)
plot(showPrevBand ? pvMS1dn : na, title = "Previous VWAP S1 - Monthly", color = #80C60099, style = plot.style_cross, transp = 10, linewidth = 3)
plot(showPrevBand ? pvQS1up : na, title = "Previous VWAP S1 - Quarterly", color = #BFA90099, style = plot.style_cross, transp = 10, linewidth = 4)
plot(showPrevBand ? pvQS1dn : na, title = "Previous VWAP S1 - Quarterly", color = #BFA90099, style = plot.style_cross, transp = 10, linewidth = 4)
plot(showPrevBand ? pvYS1up : na, title = "Previous VWAP S1 - Yearly", color = #FF8C0099, style = plot.style_cross, transp = 10, linewidth = 4)
plot(showPrevBand ? pvYS1dn : na, title = "Previous VWAP S1 - Yearly", color = #FF8C0099, style = plot.style_cross, transp = 10, linewidth = 4)

// Labels

textColor = textC == "White" ? #D0D0D0FF : textC == "Black" ? #000000FF : textC == "Red" ? #D00000FF : textC == "Green" ? #00D000FF : #0000D0FF
labelSize = size == 0 ? size.auto : size == 1 ? size.tiny : size == 2 ? size.small : size == 3 ? size.normal : size == 4 ? size.large : size.huge
lineStyle = lstyle == "Solid" ? line.style_solid : lstyle == "Dotted" ? line.style_dotted : line.style_dashed

getLabel(x, price, prefix, col, textCol, isUp, newSession) =>
    var label l = na

    if not na(price) and x > 0
        if na(l)
            l := label.new(x, price, color = col, textcolor = textCol, size = labelSize)

        p = round(price / syminfo.mintick) * syminfo.mintick

        label.set_x(l, x)
        label.set_y(l, price)
        label.set_style(l, isUp ? label.style_labeldown : label.style_labelup)
        label.set_text(l, prefix + "\n" + tostring(p))

getCurrentLine(x, price, col) =>
    var line l = na

    if not na(price) and x > 0
        if na(l)
            l := line.new(x, price, bar_index, price, extend = extend.none, width = lineWidth, style = lineStyle)

        line.set_y1(l, nz(price, close[1]))
        line.set_y2(l, nz(price, close[1]))
        line.set_x1(l, x)
        line.set_x2(l, max(bar_index, 0))
        line.set_color(l, na(price) ? #00000000 : col)

dColorLabel = opacity == 100 ? color.new(dColor, 0) : opacity == 90 ? color.new(dColor, 10) : opacity == 80 ? color.new(dColor, 20) : opacity == 70 ? color.new(dColor, 30) : opacity == 60 ? color.new(dColor, 40) : opacity == 50 ? color.new(dColor, 50) : opacity == 40 ? color.new(dColor, 60) : opacity == 30 ? color.new(dColor, 70) : opacity == 20 ? color.new(dColor, 80) : opacity == 10 ? color.new(dColor, 90) : color.new(dColor, 100)
wColorLabel = opacity == 100 ? color.new(wColor, 0) : opacity == 90 ? color.new(wColor, 10) : opacity == 80 ? color.new(wColor, 20) : opacity == 70 ? color.new(wColor, 30) : opacity == 60 ? color.new(wColor, 40) : opacity == 50 ? color.new(wColor, 50) : opacity == 40 ? color.new(wColor, 60) : opacity == 30 ? color.new(wColor, 70) : opacity == 20 ? color.new(wColor, 80) : opacity == 10 ? color.new(wColor, 90) : color.new(wColor, 100)
mColorLabel = opacity == 100 ? color.new(mColor, 0) : opacity == 90 ? color.new(mColor, 10) : opacity == 80 ? color.new(mColor, 20) : opacity == 70 ? color.new(mColor, 30) : opacity == 60 ? color.new(mColor, 40) : opacity == 50 ? color.new(mColor, 50) : opacity == 40 ? color.new(mColor, 60) : opacity == 30 ? color.new(mColor, 70) : opacity == 20 ? color.new(mColor, 80) : opacity == 10 ? color.new(mColor, 90) : color.new(mColor, 100)
qColorLabel = opacity == 100 ? color.new(qColor, 0) : opacity == 90 ? color.new(qColor, 10) : opacity == 80 ? color.new(qColor, 20) : opacity == 70 ? color.new(qColor, 30) : opacity == 60 ? color.new(qColor, 40) : opacity == 50 ? color.new(qColor, 50) : opacity == 40 ? color.new(qColor, 60) : opacity == 30 ? color.new(qColor, 70) : opacity == 20 ? color.new(qColor, 80) : opacity == 10 ? color.new(qColor, 90) : color.new(qColor, 100)
yColorLabel = opacity == 100 ? color.new(yColor, 0) : opacity == 90 ? color.new(yColor, 10) : opacity == 80 ? color.new(yColor, 20) : opacity == 70 ? color.new(yColor, 30) : opacity == 60 ? color.new(yColor, 40) : opacity == 50 ? color.new(yColor, 50) : opacity == 40 ? color.new(yColor, 60) : opacity == 30 ? color.new(yColor, 70) : opacity == 20 ? color.new(yColor, 80) : opacity == 10 ? color.new(yColor, 90) : color.new(yColor, 100)

dColorLine = opacity2 == 100 ? color.new(dColor, 0) : opacity2 == 90 ? color.new(dColor, 10) : opacity2 == 80 ? color.new(dColor, 20) : opacity2 == 70 ? color.new(dColor, 30) : opacity2 == 60 ? color.new(dColor, 40) : opacity2 == 50 ? color.new(dColor, 50) : opacity2 == 40 ? color.new(dColor, 60) : opacity2 == 30 ? color.new(dColor, 70) : opacity2 == 20 ? color.new(dColor, 80) : opacity2 == 10 ? color.new(dColor, 90) : color.new(dColor, 100)
wColorLine = opacity2 == 100 ? color.new(wColor, 0) : opacity2 == 90 ? color.new(wColor, 10) : opacity2 == 80 ? color.new(wColor, 20) : opacity2 == 70 ? color.new(wColor, 30) : opacity2 == 60 ? color.new(wColor, 40) : opacity2 == 50 ? color.new(wColor, 50) : opacity2 == 40 ? color.new(wColor, 60) : opacity2 == 30 ? color.new(wColor, 70) : opacity2 == 20 ? color.new(wColor, 80) : opacity2 == 10 ? color.new(wColor, 90) : color.new(wColor, 100)
mColorLine = opacity2 == 100 ? color.new(mColor, 0) : opacity2 == 90 ? color.new(mColor, 10) : opacity2 == 80 ? color.new(mColor, 20) : opacity2 == 70 ? color.new(mColor, 30) : opacity2 == 60 ? color.new(mColor, 40) : opacity2 == 50 ? color.new(mColor, 50) : opacity2 == 40 ? color.new(mColor, 60) : opacity2 == 30 ? color.new(mColor, 70) : opacity2 == 20 ? color.new(mColor, 80) : opacity2 == 10 ? color.new(mColor, 90) : color.new(mColor, 100)
qColorLine = opacity2 == 100 ? color.new(qColor, 0) : opacity2 == 90 ? color.new(qColor, 10) : opacity2 == 80 ? color.new(qColor, 20) : opacity2 == 70 ? color.new(qColor, 30) : opacity2 == 60 ? color.new(qColor, 40) : opacity2 == 50 ? color.new(qColor, 50) : opacity2 == 40 ? color.new(qColor, 60) : opacity2 == 30 ? color.new(qColor, 70) : opacity2 == 20 ? color.new(qColor, 80) : opacity2 == 10 ? color.new(qColor, 90) : color.new(qColor, 100)
yColorLine = opacity2 == 100 ? color.new(yColor, 0) : opacity2 == 90 ? color.new(yColor, 10) : opacity2 == 80 ? color.new(yColor, 20) : opacity2 == 70 ? color.new(yColor, 30) : opacity2 == 60 ? color.new(yColor, 40) : opacity2 == 50 ? color.new(yColor, 50) : opacity2 == 40 ? color.new(yColor, 60) : opacity2 == 30 ? color.new(yColor, 70) : opacity2 == 20 ? color.new(yColor, 80) : opacity2 == 10 ? color.new(yColor, 90) : color.new(yColor, 100)

getLabel(max(0, bar_index - offset), showPrice and dLabel ? vD : na, "VWAP" + (showSessType ? " (14Day)" : ""), dColorLabel, textColor, close[1] < vD, newSessionD)
getLabel(max(0, bar_index - offset), showPrice and wLabel ? vW : na, "VWAP" + (showSessType ? " (Week)" : ""), wColorLabel, textColor, close[1] < vW, newSessionD)
getLabel(max(0, bar_index - offset), showPrice and mLabel ? vM : na, "VWAP" + (showSessType ? " (Month)" : ""), mColorLabel, textColor, close[1] < vM, newSessionD)
getLabel(max(0, bar_index - offset), showPrice and qLabel ? vQ : na, "VWAP" + (showSessType ? " (60Day)" : ""), qColorLabel, textColor, close[1] < vQ, newSessionD)
getLabel(max(0, bar_index - offset), showPrice and yLabel ? vY : na, "VWAP" + (showSessType ? " (Year)" : ""), yColorLabel, textColor, close[1] < vY, newSessionD)

getCurrentLine(max(0, bar_index - offset), showPrice and dLabel and offset > 0 ? vD : na, dColorLine)
getCurrentLine(max(0, bar_index - offset), showPrice and wLabel and offset > 0 ? vW : na, wColorLine)
getCurrentLine(max(0, bar_index - offset), showPrice and mLabel and offset > 0 ? vM : na, mColorLine)
getCurrentLine(max(0, bar_index - offset), showPrice and qLabel and offset > 0 ? vQ : na, qColorLine)
getCurrentLine(max(0, bar_index - offset), showPrice and yLabel and offset > 0 ? vY : na, yColorLine)

// end
